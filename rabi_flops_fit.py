import numpy as np
from lmfit import Model
try:
    from Detect_peaks import detect_peaks
except Exception as ep:
    print (ep)
    def detect_peaks(y,mph,mpd=10):
        from peakutils import indexes
        return indexes(y, thres=mph, min_dist=mpd) 
import matplotlib.pyplot as plt 

def population_phonum(t,phonon,rabi_freq,cycle=1000):
    eta = 0.06
    ph = lambda n, nph : (nph/(1+nph))**n
    rabi = 2*np.pi*rabi_freq # convert linear frequency to angular freq
    omega_n = lambda n, omega : (1- eta*eta*n)*omega
    total_sum = np.array([ph(j,phonon)*np.sin(omega_n(j,rabi)*t/2)**2 for j in range(cycle)]).sum(axis=0)
    
    return total_sum / np.max(total_sum)

def cut_off_rabi_flops(phonon,rabi_freq,isplot=False):
    '''
    Estimate the cut-off time for the fits
    '''
    from Detect_peaks import detect_peaks
    dummyx = np.linspace(0,1000,2000)
    fit_function = population_phonum(dummyx,phonon,rabi_freq) - 0.5
    peaks = detect_peaks(fit_function,mph=0.01,mpd=10)
    xpeak = dummyx[peaks] - dummyx[peaks][0]
    ypeak = fit_function[peaks]
    cutoff = (1-np.exp(-1))/2
    xcutoff = dummyx[np.where(np.interp(dummyx,xpeak,ypeak)>cutoff)[0][-1]]
    if isplot:
        plt.plot(xpeak,ypeak,'o')
        plt.plot(dummyx,np.interp(dummyx,xpeak,ypeak),'-')
        plt.axhline(y=cutoff,ls='--',color='black')
        plt.axvline(x=xcutoff,ls='--',color='black')
    return xcutoff


def Rabi_flop(t,rabi_freq,detuning):
    '''
    Parameters
    1. t : pulse duration
    2. rabi_freq: rabi_freq in units of t^{-1} (linear frequency)
    3. detuning: two-level system detuning, in units of t^{-1} (linear frequency)
    '''
    rabi_eff = np.sqrt(rabi_freq**2 + detuning**2)
    return ((rabi_freq/rabi_eff)*np.sin(2*np.pi*rabi_eff*t/2))**2

def fit_rabi_flop(x,y,isplot=False,fitreport=False):

    
    peaks = detect_peaks(y,mpd=5,mph=max(y)/2)

    if len(peaks)>1:
        tpi = x[peaks][0]
        amp = y[peaks][0]
    else:
        tpi = x[np.argmax(y)]
        amp = max(y)
    
    detuning = (1/tpi)*np.sqrt(1-amp)/2 #np.sqrt(2)
    rabi_freq = (1/tpi)*np.sqrt(amp)/2 # np.sqrt(2)
    mod = Model(Rabi_flop)
    params = mod.make_params(rabi_freq=rabi_freq, detuning = detuning)
    params['rabi_freq'].set(min=1e-15,vary=not False)
    params['detuning'].set(min=1e-16,value=0,vary=False)
    try:
        result = mod.fit(y,params,t=x)
        if fitreport:
            print (result.fit_report())
        if isplot:
            f = plt.figure()
            ax = f.add_subplot(111)
            ax.plot(x,y,'-',alpha=.5)
            dummyx = np.linspace(min(x),max(x),1000)
            ax.plot(dummyx,mod.eval(result.params,t=dummyx))
#             ax.plot(dummyx,mod.eval(params,t=dummyx),ls='--')
        return result,mod
    except Exception as ep:
        print (ep)
        print ("Fit failed!")
        if isplot:
            f = plt.figure()
            ax = f.add_subplot(111)
            ax.plot(x,y,'o-',zorder=1)
            dummyx = np.linspace(min(x),max(x),1000)
            ax.plot(dummyx,mod.eval(params,t=dummyx))
        return None


def fit_population_with_phonon(x,y,isplot=False,fitreport=False):

    
    peaks = detect_peaks(y,mpd=10,mph=max(y)/2)

    if len(peaks)>1:
        tpi = x[peaks][0]
        amp = y[peaks][0]
    else:
        tpi = x[np.argmax(y)]
        amp = max(y)
    

    rabi_freq = (1/tpi)*np.sqrt(amp)/2 
    
    mod = Model(population_phonum)
    params = mod.make_params(phonon=10, rabi_freq=rabi_freq,cycle=1000)
    params['cycle'].set(vary=False)
    params['rabi_freq'].set(min=1e-8)
    params['phonon'].set(min=1e-8)
    result = mod.fit(y,params,t=x)
    if isplot:
        dummyx  = np.linspace(min(x),max(x),1000)
        plt.plot(dummyx,mod.eval(result.params,t=dummyx))
        plt.plot(x,y,'o')
    if fitreport:
        print (result.fit_report())
    return result, mod 

def rabi_flop_damped(t,rabi_freq,damp,const):
    rabi_freq *= 2*np.pi
    return const + (1-(1-damp*(rabi_freq*t)**2)*np.cos(rabi_freq*t))/2

def fit_rabi_flop_damped(x,y,const_zero=True,isplot=False,fitreport=False):

    
#     peaks = detect_peaks(y,mpd=10,mph=max(y)/2)

#     if len(peaks)>1:
#         tpi = x[peaks][0]
#         amp = y[peaks][0]
#     else:
#         tpi = x[np.argmax(y)]
#         amp = max(y)
    

#     rabi_freq = (1/tpi)*np.sqrt(amp)/2 
    rabi_freq = guess_freq(x,y)/np.pi
    mod = Model(rabi_flop_damped)
    params = mod.make_params(damp=1e-3, rabi_freq=rabi_freq,const=1e-3)
    if const_zero:
        params['const'].set(value=0,vary=False)
        
    params['rabi_freq'].set(min=1e-8)
    params['damp'].set(min=1e-9)
    result = mod.fit(y,params,t=x)
    if isplot:
        dummyx  = np.linspace(min(x),max(x),1000)
        plt.plot(dummyx,mod.eval(result.params,t=dummyx))
        plt.plot(x,y,'o')
    if fitreport:
        print (result.fit_report())
    return result, mod 

def guess_freq(x,y):
    '''
    For an oscillating signal, return the frequency of the oscillation in angular freq.
    
    '''
    X = np.abs(np.fft.fft(y))

    fs = np.diff(x)[0]
    N = x.size
    freq  = np.fft.fftfreq(n=N,d=fs)

    ind_max = np.argmax(X[1:int(N/2)])
    f0_ = freq[ind_max+1] 
    peakpos = 1/f0_
    return 2*np.pi/(2*peakpos)