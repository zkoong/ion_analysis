import numpy as np
import matplotlib.pyplot as plt
import os
import time
from glob import glob1
import pandas as pd
from scipy.io import loadmat,savemat
import peakutils 
import seaborn as sns
from datetime import datetime


class read_influx_data:
    def __init__(self,filename):
        df = pd.read_csv(filename,delimiter='\t',skiprows=2)
        value = df[df.columns[1]].to_numpy()
        time_ns = df[df.columns[0]].to_numpy()
        valid = ~np.isnan(value)
        self.time_s = np.array(time_ns[valid],dtype='float')/1e9
        self.value = np.array(value[valid],dtype='float')
        self.start_time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(self.time_s[0]))

        
    def get_data(self):
        return self.time_s,self.value
    
    def plot_data(self,time_range=None,ylabel=''):
        if time_range is None:
            f = plt.figure()
            ax = f.add_subplot(111)
            sns.set_theme(style="whitegrid", palette="pastel")
            sns.set_context("talk")
            ax.plot(self.time_s-self.time_s[0],self.value)
            ax.set_ylabel(ylabel)
            ax.set_xlabel('Time - %s (s)'%(start_time_str))
            
        else:
            epoch = datetime(1970, 1, 1)
            start_time = ((time_range[0] - epoch).total_seconds())
            end_time = ((time_range[-1] - epoch).total_seconds())
            start = np.searchsorted(self.time_s,start_time)
            self.start_time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(self.time_s[start]))
            end = np.searchsorted(self.time_s,end_time)
            f = plt.figure()
            ax = f.add_subplot(111)
            sns.set_theme(style="whitegrid", palette="Set2")
            sns.set_context("talk")
            ax.plot(self.time_s[start:end]-self.time_s[start],self.value[start:end])
            ax.set_xlabel('Time - %s (s)'%(self.start_time_str))
            ax.set_ylabel(ylabel)

            
            
if __name__ == "__main__":
    filename = r'C:\Users\ZakKoong\Desktop\data_reflection.csv'
    data = read_influx_data(filename)
    data.plot_data(time_range=[datetime(2022, 9, 14, 12,16,1),datetime(2022, 9, 14, 13,1,42)],ylabel='Reflection (mW)')
    plt.show()