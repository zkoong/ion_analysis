import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.ticker
import pandas as pd
try:
    from fourier import fourier2
except Exception as ep:
    print (ep)
    pass
def bin_logscale_data(x,y,retain=50,scale=30):
    '''
    parameters: x, y, retain (keep the first n data), scale (numbin = total/scale)
    
    '''
    from Binning_scipy import bin_data_given_bin_center
    index = np.where(x==0)[0][0]+1
    retain = int(retain)
    outx = x[index+retain:]
    outy = y[index+retain:]
    length = len(outx)
    binx,biny = bin_data_given_bin_center(np.log10(outx),outy,nbin=int(length/scale),opt='mean')
    valid = ~np.isnan(biny)
    binx = np.concatenate([x[index:index+retain],10**binx[valid]])
    biny = np.concatenate([y[index:index+retain],biny[valid]])
    return binx,biny

class timetagging_beat:
    def __init__(self,filename,ratio=1e-12,phase=True,plot=False):
        '''
        Input: beat filename
        phase = True (default) : execute the phase difference analysis
        
        Execute fft straightaway upon initialization.
        '''
        self.filename = filename
        self.timetags = self.fasterloadtxt()* ratio
        self.timetags -= self.timetags[0]
        self.length_data = len(self.timetags)
        self.total_time = self.timetags[-1]
        self.plot = plot
        self.average_freq = (self.length_data)/self.total_time
        if phase:
            self.mode = "Phase_difference"
            print ('Analysis mode is %s'%self.mode)
            self.phase_difference_timetrace()
        else:
            self.mode = "Time trace"
            print ('Analysis mode is %s'%self.mode)
            self.counts_timetrace()
    
    def getfftdata(self):
        if self.mode == "Phase_difference":
            return  self.freq_phase,self.amp_phase
        else:
            return  self.freq_count,self.amp_count
        
    def fasterloadtxt(self):
        '''
        using pandas to speed up file reading
        '''
        df = pd.read_csv(self.filename,sep=' ', header=None)
        return df.values.flatten()
    
    def plotfft(self,xlog=True,xlim=None,ylim=None,title=None):
        f = plt.figure()
        ax = f.add_subplot(111)
        if self.mode == "Phase_difference":
            ax.plot(self.freq_phase,self.amp_phase,'-',color='navy',lw=2.5)
            tempx = self.freq_phase
            tempy = self.amp_phase
        else:
            ax.plot(self.freq_count,self.amp_count,'-',color='navy',lw=2.5)
            tempx = self.freq_count
            tempy = self.amp_count
            
        ax.set_xlabel('Freq. (Hz)')
        ax.set_ylabel('FFT')
        ax.set_yscale('log')
        if xlim is not None:
            ax.set_xlim(xlim)
            start = np.searchsorted(tempx,xlim[0])
            end = np.searchsorted(tempx,xlim[-1])
            bottom = np.min(tempy[start:end])
            top = np.max(tempy[start:end])
            ax.set_ylim(bottom,top)
        if ylim is not None:
            ax.set_ylim(ylim)
        if xlog:
            ax.set_xscale('log')
            locmaj = matplotlib.ticker.LogLocator(base=10.0, subs=(1.0, ), numticks=100)
            locmin = matplotlib.ticker.LogLocator(base=10.0, subs=np.arange(1, 10) * .1,numticks=100)
            ax.xaxis.set_major_locator(locmaj)
            ax.xaxis.set_minor_locator(locmin)
            ax.xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
        ax.grid('on',which='both',alpha=0.5)
        if title is not None:
            ax.set_title(title)
        return f
        
    def phase_difference_timetrace(self):
        phi = 2*np.pi*np.arange(self.length_data) # phase at time t
        phi0= 2*np.pi*self.average_freq*self.timetags;   # phases of a perfectly stable oscillator
        self.phase_diff = phi - phi0

        phase_y = self.phase_diff/2/np.pi
        
        freq_phase,amp_phase = fourier2(self.timetags,phase_y)
        self.freq_phase = freq_phase
        self.amp_phase = amp_phase
        # self.freq_phase,self.amp_phase = bin_logscale_data(freq_phase,amp_phase,retain=30,scale=1000)
        if self.plot:
            self.plotfft()
        
    def counts_timetrace(self,binsize=1e-5):
        raw_data = self.timetags
        timebin = np.arange(binsize,np.ceil(self.total_time)+binsize/2,binsize)
        countbin = np.zeros(len(timebin))
        cnt = 0
        end_of_bin = timebin[cnt]

        for j in range(len(raw_data)):
            if raw_data[j] <= end_of_bin:
                countbin[cnt] += 1
            else:
                cnt += 1
                end_of_bin = timebin[cnt]
                if raw_data[j] <= end_of_bin:
                    countbin[cnt] += 1
        freq_count,amp_count = fourier2(timebin,countbin)
        self.freq_count,self.amp_count = bin_logscale_data(freq_count,amp_count,retain=10,scale=500)
        if self.plot:
            self.plotfft()

