import numpy as np
import matplotlib.pyplot as plt
from glob import glob1
import os
import pandas as pd
from datetime import datetime
import time

import matplotlib
matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['lines.linewidth'] = 3
matplotlib.rcParams['font.family']='arial'
params = {'legend.edgecolor': '0',
          'lines.markersize' : 5,
          'legend.borderaxespad': 1.5,
          'legend.fancybox': False,
          'legend.fontsize': 32.0,
          'legend.framealpha': 0.5,
          'legend.labelspacing': 0.3,
          'legend.markerscale': 1.0,
          'figure.figsize': (10, 8),
         'axes.labelsize': 30,
         'axes.titlesize': 30,
         'axes.linewidth': 3,
         'axes.xmargin': 0.03,
         'axes.ymargin': 0.03,
         'xtick.direction': 'in',
         'xtick.labelsize': 30,
         'xtick.major.pad': 10,
         'xtick.major.size': 10,
         'xtick.major.width': 3,
         'xtick.minor.pad': 10,
         'xtick.minor.size': 5,
         'xtick.minor.visible': True,
         'xtick.minor.width': 2,
         'xtick.top': True,
         'ytick.direction': 'in',
         'ytick.labelsize': 30,
         'ytick.major.pad': 10,
         'ytick.major.size': 10,
         'ytick.major.width': 3,
         'ytick.minor.pad': 10,
         'ytick.minor.size': 5,
         'ytick.minor.visible': True,
         'ytick.minor.width': 2,
         'ytick.right': True,}
plt.rcParams.update(params)

def read_spectrum_analyzer_csv(filename,spectrograph=False,isplot=False,issaved=False):
    if not spectrograph:
        df = pd.read_csv(filename,sep=';',skiprows=6)
        list_of_column_names = df.keys()

        freq = df[list_of_column_names[0]].str.replace(',', '.')
        amplitude = df[list_of_column_names[1]].str.replace(',', '.')

        freqoffset = amplitude[np.where(freq == "Freq Offset")[0][0]]
        reflevel_dBm = amplitude[np.where(freq == "Ref Level")[0][0]]
        start = np.where(freq == "Values")[0][0]+1
        stop = np.where(freq == "Trace")[0][1]

        freq = np.array(freq[start:stop],dtype='float')  + float(freqoffset)
        amplitude = np.array(amplitude[start:stop],dtype='float') + float(reflevel_dBm)

        if isplot:
            f = plt.figure(figsize=(10,10))
            f.subplots_adjust(hspace=0.4)
            ax1 = f.add_subplot(211)
            ax = f.add_subplot(212)

            ax.semilogx(freq,amplitude,color='navy')
            ax.set_xlabel('Freq. (Hz)')
            ax.set_ylabel('Amplitude (dBm)')

            ax1.plot(freq,amplitude,color='navy')
            ax1.set_xlabel('Freq. (Hz)')
            ax1.set_ylabel('Amplitude (dBm)')
            if issaved:
                f.savefig(filename[:-4]+'_data.png',dpi=100,bbox_inches='tight',facecolor='white')
                print ('Figure saved as %s'%(filename[:-4]+'_data.png'))
        return freq, amplitude
    else:
        df = pd.read_csv(filename,sep=';',skiprows=6,decimal=",")
        list_of_column_names = df.keys()

        freq = df[list_of_column_names[0]].str.replace(',', '.')
        amplitude = df[list_of_column_names[1]].str.replace(',', '.')
        timestamps = df[list_of_column_names[2]].str.replace(',', '.')
        
        freqoffset = amplitude[np.where(freq == "Freq Offset")[0][0]]
        reflevel_dBm = amplitude[np.where(freq == "Ref Level")[0][0]]
        frame_start_index = np.where(freq == "Timestamp")[0]
        start = np.where(freq == "Values")[0][0]+1
        stop = np.where(freq == "Trace")[0][1]


        ## time stamps

        time_raw = np.array(timestamps[frame_start_index])
        time_s = np.zeros(len(time_raw))
        t0 = datetime.strptime(time_raw[0], '%H:%M:%S.%f')
        for j in range(1,len(time_raw),1):
            time_s[j] = (datetime.strptime(time_raw[j], '%H:%M:%S.%f')-t0).total_seconds()

        ## histogram traces array
        freq_ary = np.array(freq[start:stop],dtype='float')  + float(freqoffset)
        amplitude_ary = np.zeros((len(time_raw),len(freq_ary)))
        for j in range(len(time_raw)):
            amplitude_ary[j] = np.array(amplitude[frame_start_index[j]+1:frame_start_index[j]+1+len(freq_ary)],dtype='float')  + float(reflevel_dBm)
        amplitude1 = np.array(amplitude[start:stop],dtype='float') + float(reflevel_dBm)
        if isplot:
            f = plt.figure(figsize=(10,10))
            f.subplots_adjust(hspace=0.05)
            ax = f.add_subplot(211)
            ax.plot(freq_ary,amplitude1,color='navy')
            ax.set_ylabel('Amplitude (dBm)')
            ax.set_xticklabels([])

            ax1 = f.add_subplot(212)
            im = ax1.pcolormesh(freq_ary,time_s- time_s[-1],amplitude_ary,cmap='inferno_r')
            cax = f.add_axes([0.57, 0.2, 0.3, 0.05])
            cbar = f.colorbar(im, cax=cax,orientation='horizontal')
            cbar.ax.set_title('Amplitude (dBm)')
            ax1.set_xlabel('Freq. (Hz)')
            ax1.set_ylabel('Time (s)')
            if issaved:
                f.savefig(filename[:-4]+'_data.png',dpi=100,bbox_inches='tight',facecolor='white')
                print ('Figure saved as %s'%(filename[:-4]+'_data.png'))
        return freq_ary,time_s- time_s[-1],amplitude_ary
    

if __name__=="__main__":
    directory = r'Z:\qfc\users\Armin\spectrum_analyser_traces'
    flst = glob1(directory,'**.csv')
    for j in range(len(flst)):
        filename = os.path.join(directory,flst[j])
        print (flst[j])
        try:
            freq,amplitude = read_spectrum_analyzer_csv(filename,isplot=True,issaved=False)
        except:
            print ('Spectrograph mode')
            freq,time_ary,amplitude = read_spectrum_analyzer_csv(filename,spectrograph=True,isplot=True,issaved=False)

    plt.close('all')