import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from datetime import datetime
import matplotlib
matplotlib.rcParams.update({'font.size': 30})
matplotlib.rcParams['lines.linewidth'] = 3
matplotlib.rcParams['font.family']='arial'
params = {'legend.edgecolor': '0',
          'lines.markersize' : 10,
          'legend.borderaxespad': 1.5,
          'legend.fancybox': False,
          'legend.fontsize': 25.0,
          'legend.framealpha': 0.5,
          'legend.labelspacing': 0.3,
          'legend.markerscale': 1.0,
          'figure.figsize': (10, 8),
         'axes.labelsize': 30,
         'axes.titlesize': 30,
         'axes.linewidth': 3,
         'axes.xmargin': 0.03,
         'axes.ymargin': 0.03,
         'xtick.direction': 'in',
         'xtick.labelsize': 30,
         'xtick.major.pad': 10,
         'xtick.major.size': 10,
         'xtick.major.width': 3,
         'xtick.minor.pad': 10,
         'xtick.minor.size': 5,
         'xtick.minor.visible': True,
         'xtick.minor.width': 2,
         'xtick.top': True,
         'ytick.direction': 'in',
         'ytick.labelsize': 30,
         'ytick.major.pad': 10,
         'ytick.major.size': 10,
         'ytick.major.width': 3,
         'ytick.minor.pad': 10,
         'ytick.minor.size': 5,
         'ytick.minor.visible': True,
         'ytick.minor.width': 2,
         'ytick.right': True,}
plt.rcParams.update(params)

directory  = r'Z:\qfc\measurements\general\2022\cavity_temperature_log'
temp_data = os.path.join(directory,'June2022_Temp_Log.dat')


temperature_data = pd.read_csv(temp_data,delimiter='\t',skiprows=1,header=None)
temperature_celcius = np.array(temperature_data[temperature_data.keys()[2]])
time_day = np.array(temperature_data[temperature_data.keys()[1]])
dates = np.array(temperature_data[temperature_data.keys()[0]])
time_s = np.zeros(len(dates))
next_day = [0]
for j in range(len(dates)):
    try:
        if dates[j-1] != dates[j]:
            next_day.append(j)
    except:
        pass
    _temp = '%s %s'%(dates[j],time_day[j])
    epoch = datetime(1970, 1, 1)
    formats = "%d/%m/%Y %H:%M:%S"
    time_s[j] = (datetime.strptime(_temp, formats) - epoch).total_seconds()
time_zero = datetime.fromtimestamp(time_s[next_day[-1]]).strftime( "%Y-%m-%d %H:%M:%S" )
time_s -= time_s[next_day[-1]]
start = np.searchsorted(time_s,0)
time_s = time_s[start:]
temperature_celcius =temperature_celcius[start:]
###
#calibrate frequency shift

beat_freq_shift_vs_temp = os.path.join(directory,'Beat_freq_MHz_vs_Temperature_degCel.csv')
freq_shift = pd.read_csv(beat_freq_shift_vs_temp,header=None)
temp = freq_shift[0][:-1]
freq_shift_Mhz = freq_shift[1][:-1]
from scipy.interpolate import interp1d
freq_shift_func = interp1d(temp,freq_shift_Mhz,kind='cubic')
from lmfit.models import PolynomialModel
mod = PolynomialModel(degree=2)
params = mod.guess(freq_shift_Mhz,x=temp)
result = mod.fit(freq_shift_Mhz,params,x=temp)

###
f = plt.figure()
ax = f.add_subplot(111)
ax1 = ax.twinx()
ax1.plot(time_s/3600, 1e3*mod.eval(result.params,x=temperature_celcius)-1e3*mod.eval(result.params,x=32.7),'s',
         color='darkorange',zorder=0,alpha=0.5,markersize=5)
ax1.set_ylabel('Est. Freq. Shift (kHz)',color='darkorange')
ax.plot(time_s/3600,temperature_celcius,'o',color='navy',zorder=1,markersize=5)
ax.set_xlabel('Time - %s GMT (hour)'%(time_zero))
ax.set_ylabel('Temperature ($^\circ$C)',color='navy')
ax.set_xlim(left=0,right=1.01*max(time_s/3600))
ax.axvline(x=0.45,color='black',ls='--',label='Restart',zorder=0)

# ax.set_xlim(left=0,right=9);
# ax.axvline(x=4.875,color='black',ls='--',zorder=0)
# ax.axvline(x=5.2,color='black',ls='--',label='Restart',zorder=0)
ax.legend(loc='best')
f.savefig(temp_data[:-3]+'_20220610_summary.png',dpi=100,bbox_inches='tight',facecolor='white')