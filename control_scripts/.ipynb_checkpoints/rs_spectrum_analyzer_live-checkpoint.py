'''
Live beat data from a spectrum analyzer with gaussian fits.
'''

from re import L
from rohdeschwarzfpc import FPC
import numpy as np
import time
import matplotlib.pyplot as plt
import os
from lmfit.models import GaussianModel

import matplotlib

matplotlib.rcParams.update({'font.size': 20})
matplotlib.rcParams['lines.linewidth'] = 3
matplotlib.rcParams['font.family'] = 'arial'

params = {'legend.edgecolor': '0',
          'legend.borderaxespad': 1.5,
          'legend.fancybox': False,
          'legend.fontsize': 20.0,
          'legend.framealpha': 0.5,
          'legend.labelspacing': 0.3,
          'legend.markerscale': 1.0,
          'figure.figsize': (5, 4),
          'axes.labelsize': 20,
          'axes.titlesize': 30,
          'axes.linewidth': 3,
          'axes.xmargin': 0.03,
          'axes.ymargin': 0.03,
          'xtick.direction': 'in',
          'xtick.labelsize': 20,
          'xtick.major.pad': 10,
          'xtick.major.size': 10,
          'xtick.major.width': 3,
          'xtick.minor.pad': 10,
          'xtick.minor.size': 5,
          'xtick.minor.visible': True,
          'xtick.minor.width': 2,
          'xtick.top': True,
          'ytick.direction': 'in',
          'ytick.labelsize': 20,
          'ytick.major.pad': 10,
          'ytick.major.size': 10,
          'ytick.major.width': 3,
          'ytick.minor.pad': 10,
          'ytick.minor.size': 5,
          'ytick.minor.visible': True,
          'ytick.minor.width': 2,
          'ytick.right': True, }
plt.rcParams.update(params)

def Gaussianfit(x,y,isplot=False):
    mod = GaussianModel()
    params = mod.guess(y,x=x)
    result = mod.fit(y,params,x=x)
    if isplot:
        f = plt.figure()
        ax = f.add_subplot(111)
        ax.plot(x,y,'-o',alpha=0.75,color='navy')
        dummyx = np.linspace(min(x),max(x),1000)
        ax.plot(dummyx,mod.eval(result.params,x=dummyx),color='darkorange')
        try:
            ax.set_title('FWHM = %.3f +- %.3f'%(result.params['fwhm'].value,result.params['fwhm'].stderr))
        except:
            ax.set_title('FWHM = %.3f +- %.3f'%(result.params['fwhm'].value,0))
    return result, mod

class rs_spectrum():

    def __init__(self, address='TCPIP::169.254.115.38::inst0::INSTR'):
        self.pico = FPC(address= address)    
        
    def getspectra(self,mem=False):
        self.tr = self.pico.get_trace(n=1,mem=mem)
        return self.tr['x'],self.tr['y']
 
    def save_file(self, name):
        print("Saving data")
        np.savez(name, freq=self.tr['x'],amplitude=self.tr['y'])
        print("Data Saved.")


    def close(self):
        pass
        print('Device closed.')


if __name__ == '__main__':
    
    address = 'TCPIP0::FPC1500-102158::inst0::INSTR' # FPC1500 spectrum analyzer'
    address = 'TCPIP::169.254.115.38::inst0::INSTR'
    gaufit = True
    pico_device = rs_spectrum(address)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel('Freq (Hz)')
    ax.set_ylabel('Amplitude')
    plt.ion()
    fig.show()
    fig.canvas.draw()
    while True:
        t, c = pico_device.getspectra(mem=False)
        ax.clear()
        if gaufit:
            res,mod = Gaussianfit(t,c,isplot=False)
            ax.plot(t,res.best_fit,color='darkorange')
            try:
                ax.set_title('FWHM = %.1f +- %.1f'%(res.params['fwhm'].value,res.params['fwhm'].stderr))
            except:
                ax.set_title('FWHM = %.1f +- %s'%(res.params['fwhm'].value,"n/a"))
        ax.plot(t , c, color='navy',alpha=0.5)
        plt.pause(0.3)
    pico_device.close()
    plt.show()
