import time
from matplotlib import pyplot as plt
import numpy as np

import matplotlib
matplotlib.rcParams.update({'font.size': 20})
matplotlib.rcParams['lines.linewidth'] = 3
matplotlib.rcParams['font.family']='arial'

def lorentzian(x,x0,fwhm):
    return (fwhm/2)**2/((x-x0)**2+ (fwhm/2)**2)

def live_update_demo(blit = False):
    x = np.linspace(-10,10., num=300)


    fig = plt.figure()
    ax2 = fig.add_subplot(111)


    line, = ax2.plot([], lw=3)
    text = ax2.text(0.8,0.5, "")

    ax2.set_xlim(x.min(), x.max())
    ax2.set_ylim([-0.1, 1.1])
    ax2.set_xlabel('Freq. (Hz)')
    ax2.set_ylabel('FFT')
    fig.canvas.draw()   # note that the first draw comes before setting data 


    if blit:
        # cache the background
        ax2background = fig.canvas.copy_from_bbox(ax2.bbox)

    plt.show(block=False)


    t_start = time.time()
    k=0.5

    for i in np.arange(1000):
        line.set_data(x, lorentzian(x,0,k))
        # line.set_data(x, np.sin(x/3.+k))
        #tx = 'Mean Frame Rate:\n {fps:.3f}FPS'.format(fps= ((i+1) / (time.time() - t_start)) ) 
        #text.set_text(tx)
        k+=0.01
        if blit:
            # restore background
            fig.canvas.restore_region(ax2background)

            # redraw just the points
            ax2.draw_artist(line)
            ax2.draw_artist(text)

            # fill in the axes rectangle
            fig.canvas.blit(ax2.bbox)
            # fig.canvas.update()

            # in this post http://bastibe.de/2013-05-30-speeding-up-matplotlib.html
            # it is mentionned that blit causes strong memory leakage. 
            # however, I did not observe that.

        else:
            # redraw everything
            fig.canvas.draw()

        fig.canvas.flush_events()
        #alternatively you could use
        #plt.pause(0.000000000001) 
        # however plt.pause calls canvas.draw(), as can be read here:
        #http://bastibe.de/2013-05-30-speeding-up-matplotlib.html

if __name__=="__main__":
    live_update_demo(True)   # 175 fps
    #live_update_demo(False) # 28 fps