import numpy as np
import matplotlib.pyplot as plt
from simple_histogram import histogram 

plt.rcParams['font.size'] = 16

counts1,counts2,attempts = histogram('Z:\\measurements\\QFC\\2022_02_16\\2022_02_16_162456.txt')


print(attempts)

fig, ax1 = plt.subplots()
n, bins, patches = plt.hist(counts1,np.linspace(0,200,200), alpha=0.75,label="Channel 2")
n2, bins2, patches = plt.hist(counts2,np.linspace(0,200,200), alpha=0.75,label="Channel 6")

plt.grid()
plt.legend()
plt.xlabel("Time (us)")
plt.ylabel("Counts")

bin2centers = np.array([(bins2[i] + bins2[i+1])/2 for i in range(len(bins2)-1)])
bin1centers = np.array([(bins[i] + bins[i+1])/2 for i in range(len(bins)-1)])


#Background counts


#Eff
n2 = n2[bin2centers>12]
bin2centers = bin2centers[bin2centers>12]

n2 = n2[bin2centers<150]
bin2centers = bin2centers[bin2centers<150]

n = n[bin1centers>12 ]
bin1centers = bin1centers[bin1centers>12]

n = n[bin1centers<150]
bin1centers = bin1centers[bin1centers<150]

ax2 = ax1.twinx()
ax2.plot(bin1centers,np.cumsum(n)/attempts,label="Channel 2")
ax2.plot(bin2centers,np.cumsum(n2)/attempts,label="Channel 6")
#plt.plot(bin2centers,np.cumsum(n2+n2extra)/attempts,label="Channel 6 + crosstalk")
#plt.grid()
#plt.legend()
#plt.xlabel("Time (us)")
#plt.ylabel("Photon probability")


plt.show()
