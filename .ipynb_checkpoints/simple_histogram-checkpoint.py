'''
This script aims to perform simple correlation measurements,
given time tags and the associated detection channels.

The function histogram takes a file, containing the relevant information,
i.e. timetags and channel, along with the start and stop(s) channels.
with the default being 
start = channel 0
ch1 = channel 2
ch2 = channel 7

return time difference of chn1-start, chn2-start, num_attempts

'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
textsize = 30
matplotlib.rcParams.update({'font.size': textsize})
matplotlib.rcParams['lines.linewidth'] = 3
matplotlib.rcParams['font.family']='arial'
params = {'legend.edgecolor': '0',
          'lines.markersize' : 10,
          'legend.borderaxespad': 1.5,
          'legend.fancybox': False,
          'legend.fontsize': 32.0,
          'legend.framealpha': 0.5,
          'legend.labelspacing': 0.3,
          'legend.markerscale': 1.0,
          'figure.figsize': (10, 8),
         'axes.labelsize': textsize,
         'axes.titlesize': textsize,
         'axes.linewidth': 3,
         'axes.xmargin': 0.03,
         'axes.ymargin': 0.03,
         'xtick.direction': 'in',
         'xtick.labelsize': textsize,
         'xtick.major.pad': 10,
         'xtick.major.size': 10,
         'xtick.major.width': 3,
         'xtick.minor.pad': 10,
         'xtick.minor.size': 5,
         'xtick.minor.visible': True,
         'xtick.minor.width': 2,
         'xtick.top': True,
         'ytick.direction': 'in',
         'ytick.labelsize': textsize,
         'ytick.major.pad': 10,
         'ytick.major.size': 10,
         'ytick.major.width': 3,
         'ytick.minor.pad': 10,
         'ytick.minor.size': 5,
         'ytick.minor.visible': True,
         'ytick.minor.width': 2,
         'ytick.right': True,}
plt.rcParams.update(params)

def histogram(filename,trigger=0,chn1=2,chn2=7):
    '''
    Parameter:
    1. filename : contains timetags and channels
    2. trigger : trigger channel. Default is chn 0
    3. chn1 : photon channel. Default is chn 2
    4. chn1 : photon channel. Default is chn 7


    Return
    1. time difference of chn1-trigger in us
    2. time difference of chn2-start in us
    3. num_attempts
    '''
    counts1 = np.array([])
    counts2 = np.array([])

    triggertime = 0
    attempts = 0

    with open(filename) as f:
        lines=f.readlines()
        del lines[0]
        for line in lines:
            tag = np.fromstring(line, dtype=float, sep='\t')
            tag[0] = tag[0]/1e6

            if tag[1]==trigger:
                triggertime = tag[0]
                attempts += 1
            elif tag[1]==chn1:
                counts1 = np.append(counts1,tag[0]-triggertime)
            elif tag[1]==chn2:
                counts2 = np.append(counts2,tag[0]-triggertime)

    return counts1,counts2,attempts

def plot_wavepacket(counts1, counts2, num_attempts, nbins=201,savefilename=None):
    f = plt.figure()
    ax = f.add_subplot(111)
    
    binnum, bins, patches = ax.hist(counts1,np.linspace(0,200,nbins),color='navy',label='Channel 2');
    bin1centers = np.array([(bins[i] + bins[i+1])/2 for i in range(len(bins)-1)])
    if counts2 is not None:
        binnum2, bins2, patches = ax.hist(counts2,np.linspace(0,200,nbins),color='darkgreen',label='Channel 7');
        bin2centers = np.array([(bins2[i] + bins2[i+1])/2 for i in range(len(bins2)-1)])
    
    
    
    ax.set_xlabel(r"Time ($\mathregular{\mu}$s)")
    ax.set_ylabel(r"Coincidences in %.1f $\mathregular{\mu}$s"%(np.diff(np.linspace(0,200,nbins))[0]),color='navy')
    
    ax2 = ax.twinx()
    ax2.plot(bin1centers,100*np.cumsum(binnum)/num_attempts,'-',label="Channel 2",color='darkorange')
    if counts2 is not None:
        ax2.plot(bin2centers,100*np.cumsum(binnum2)/num_attempts,'-',label="Channel 7",color='red')
    ax2.set_ylabel("Photon probability (%)",color='darkorange')
    ax2.set_ylim(bottom=0)
    ax.set_ylim(bottom=0)
    ax.legend(frameon=False,loc='best')
    if savefilename is not None:
        f.savefig(savefilename,dpi=100,bbox_inches='tight',facecolor='white')
        np.savetxt(savefilename[:-4]+'.txt',np.transpose([bin1centers,binnum]))