import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import linalg
plt.style.use('ggplot')
import matplotlib
textsize = 15
matplotlib.rcParams.update({'font.size': textsize})
matplotlib.rcParams['lines.linewidth'] = 2
matplotlib.rcParams['font.family']='arial'

if __name__ =="__main__":

    filename = r'C:\Users\zxk94\Desktop\dpr_30_405_24_5_vin_vout_vmon.txt'
    import pandas as pd
    from lmfit.models import LinearModel

    df = pd.read_csv(filename,delimiter='\t')

    vset = df[df.columns[0]]
    vout = df[df.columns[1]]
    vmon = df[df.columns[2]]

    f = plt.figure(figsize=(10,12))
    f.subplots_adjust(hspace=0.3)
    ax  =f.add_subplot(311)
    ax1 = f.add_subplot(312)
    ax2 = f.add_subplot(313)
    ax.plot(vset,vout,'o',zorder=1)
    ax.set_ylabel('Vout (V)')
    ax1.plot(vset,vmon,'o',zorder=1)
    ax1.set_ylabel('Vmon (V)')
    ax.set_xlabel('Vset (V)')
    ax1.set_xlabel('Vset (V)')

    ax2.plot(vmon,vout,'o',zorder=1)
    ax2.set_xlabel('Vmon (V)')
    ax2.set_ylabel('Vout (V)')
    mod = LinearModel()
    params = mod.guess(vout,x=vset)
    result = mod.fit(vout,params,x=vset)
    dummyx = np.linspace(0,max(vset),1000)
    ax.plot(dummyx,mod.eval(result.params,x=dummyx),zorder=0,
            label='Vout = %.1f (%.0f) Vset + %.1f (%.0f)'%(result.params['slope'].value,
                                                           result.params['slope'].stderr*1e1,
                                                            result.params['intercept'].value,
                                                          result.params['intercept'].stderr*1e1))

    mod = LinearModel()
    params = mod.guess(vmon,x=vset)
    result = mod.fit(vmon,params,x=vset)
    dummyx = np.linspace(0,max(vset),1000)
    ax1.plot(dummyx,mod.eval(result.params,x=dummyx),zorder=0,
             label='Vmon = %.3f (%.0f) Vset + %.3f (%.0f)'%(result.params['slope'].value,
                                                     result.params['slope'].stderr*1e3,
                                                     result.params['intercept'].value,
                                                    result.params['intercept'].stderr*1e3))

    mod = LinearModel()
    params = mod.guess(vout,x=vmon)
    result = mod.fit(vout,params,x=vmon)
    dummyx = np.linspace(0,max(vmon),1000)
    ax2.plot(dummyx,mod.eval(result.params,x=dummyx),zorder=0,
             label='Vout = %.1f (%.0f) Vmon + %.3f (%.0f)'%(result.params['slope'].value,
                                                     result.params['slope'].stderr*1e1,
                                                     result.params['intercept'].value,
                                                    result.params['intercept'].stderr*1e1))

    ax.set_xlim(left=0)
    ax.legend(loc='best')
    ax1.set_xlim(left=0)
    ax1.legend(loc='best')
    ax2.set_xlim(left=0)
    ax2.legend(loc='best')
    f.savefig(filename[:-4]+'_summary.png',dpi=100,bbox_inches='tight',facecolor='white')