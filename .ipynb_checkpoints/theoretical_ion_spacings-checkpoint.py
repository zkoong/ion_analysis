import numpy as np

def theoretical_ion_spacing_um(numions=10,endcap_voltages=[250,265]):
    '''
    Return the array containing the ion positions
    given the numions (default = 10) and the endcap_voltages in V
    (default = 250,265)
    '''
    def ion_spacing_prefactor_um(omega_linear_MHz):
        u = 1.66053906660e-27
        ec = 1.602176634e-19
        epsilon = 8.8541878128e-12
        mass = 40.078*u
        Z = 1
        omega = 2*np.pi*omega_linear_MHz*1e6
        return 1e6*(Z*Z*ec*ec/4/np.pi/epsilon/mass/omega/omega)**(1./3)
    
    axial_com_MHz = lambda average_electrode_voltages : 0.02887*(average_electrode_voltages)**0.5
    
    twoions_position = np.array([-0.62996, 0.62996])
    threeions_position = np.array([-1.0772,0,1.0772])
    fourions_position = np.array([-1.4368,-0.45438, 0.45438, 1.4368])
    fiveions_position = np.array([-1.7429,-0.8221, 0, 0.8221, 1.7429])
    sixions_position = np.array([-2.0123, -1.1361, -0.36992, 0.36992, 1.1361, 2.0123])
    sevenions_position = np.array([-2.2545, -1.4129, -0.68694, 0, 0.68694, 1.4129, 2.2545])
    eightions_position = np.array([-2.4758, -1.6621, -0.96701, -0.31802, 0.31802, 0.96701, 1.6621, 2.4758])
    nineions_position = np.array([-2.6803, -1.8897, -1.2195, -0.59958, 0, 0.59958, 1.2195, 1.8897, 2.6803])
    tenions_position = np.array([-2.8708, -2.10003, -1.4504, -0.85378, -0.2821, 0.2821, 0.85378, 1.4504, 2.10003, 2.8708])
    ion_position = {2:twoions_position,
                   3: threeions_position,
                   4: fourions_position,
                   5: fiveions_position,
                   6: sixions_position,
                   7: sevenions_position,
                   8: eightions_position,
                   9: nineions_position,
                   10: tenions_position}
    
    prefactor = ion_spacing_prefactor_um(axial_com_MHz(np.mean(endcap_voltages)))
    return ion_position[int(numions)]*prefactor
    
    